<!-- Footer -->
<footer>
<div class="container h-100">
    <div class="row">
        <div class="col-lg-12 p-0 d-flex justify-content-between footer-menu-content">
            <div class="col-md-12 col-lg-2">
                <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
            </div>
            <div class="col-md-10 d-flex justify-content-end">
                <ul class="footer-menu">         
                    <li>
                        <a href="<?php echo home_url(); ?>/quem-somos/" target="" class="pages">Quem somos</a>
                        
                        <div class="sub-menu">
                            <a href="/quem-somos#missao-e-valores">Visão e missão</a>
                            <a href="/quem-somos#movimento">Empresas participantes</a>
                            <a href="/quem-somos#parceiros">Parceiros</a>
                        </div>
                    </li>
                    <li>                                                                                                   
                        <a href="<?php echo home_url(); ?>/associe-se/" target="" class="pages">Associe-se</a>
                    
                        <div class="sub-menu">
                            <a href="/associe-se#quero-fazer-parte">Quero fazer parte</a>
                            <a href="/associe-se#informacao">Compromisso e adesão ao MISEC</a>
                            <a href="/duvidas-frequentes">Dúvidas frequentes - MISEC</a>
                        </div>
                    </li>                                                                                                        
                    <li>                                                                                                       
                        <a href="<?php echo home_url(); ?>/capacitacao-em-compliance/" target="" class="pages">Capacitação em Compliance</a>
                        
                        <div class="sub-menu">
                            <a href="/capacitacao-em-compliance#avalie">Avalie o seu programa de integridade</a>
                            <a href="/capacitacao-em-compliance#treinamento">Vídeos e treinamentos</a>
                            <a href="/capacitacao-em-compliance#cartilhas">Cartilhas</a>
                            <a href="/capacitacao-em-compliance#cartilhas">Agenda de eventos</a>
                            <a href="/capacitacao-em-compliance#noticias-e-artigos">Artigos</a>
                        </div>
                    </li>
                    <li>                                                                                                       
                        <a href="<?php echo home_url(); ?>/contato/" target="" class="pages">Contato</a>
                        
                        <div class="sub-menu">
                            <a href="/contato#fale-conosco">Fale Conosco</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12 direitos">
            <p>Todos os direitos reservados a <strong>Open</strong></p>
        </div>
    </div>
</div>

</footer>

  
<?php wp_footer(); ?>

</body>

</html>
