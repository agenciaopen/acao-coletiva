<!DOCTYPE html>
<?php
$current_user = wp_get_current_user();
if ( ! user_can( $current_user, "subscriber" ) ){ // Check user object has not got subscriber role
    $user_logged = 'not';
} else {
$user_logged = 'subs';
?>
<style>
  .subs .menupop, .subs #wpadminbar>#wp-toolbar>#wp-admin-bar-top-secondary>#wp-admin-bar-search #adminbarsearch input.adminbar-input {
    display: none;
}
li#wp-admin-bar-my-account{
  display: block !important;
}
li#wp-admin-bar-my-account img{
  display: none;
}
/* html.subs{
  margin-top: 0 !important;
} */
</style>
<?php }?>

<html <?php language_attributes(); ?> class="<?php echo $user_logged;?>">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <title><?php wp_title(); ?></title>

        <?php wp_head(); ?>

    </head>


<body <?php body_class( $user_logged); ?> id="misec">
<?php

if ( wp_is_mobile() ) :
    $logo_site = get_field( 'logo_site_mobile', 'option' );
else :
    $logo_site = get_field( 'logo_site', 'option' );
endif;

?>

   
    <nav class="navbar navbar-expand-xl navbar-default fixed-top" role="navigation" id="nav_main">
      <div class="container h-100">
        <!-- Brand and toggle get grouped for better mobile display -->
        
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img src='<?php echo $logo_site ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
        </a>

        <a href="<?php echo home_url(); ?>" class="hidden-desk mr-5"> 
            <img src="<?php the_field( 'home', 'option' ); ?>" alt="">
        </a>
        <button type="button" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
            <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav ml-auto align-items-center',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ) );
            ?>
        </div>
    </nav>