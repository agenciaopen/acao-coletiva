<?php
/**
*
* Template Name: Contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="fale-conosco" id="fale-conosco">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field( 'titulo_contato' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_contato' ); ?></p><hr>
            </div>
            <div class="col-md-12 form-fale">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Fale com a gente"]');?>
            </div>
        </div>
    </div>
</section>
 
<?php get_footer(); ?>