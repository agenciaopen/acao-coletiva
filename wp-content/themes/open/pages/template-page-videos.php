<?php
/**
*
* Template Name: Videos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="galeria-de-videos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="videos">
					<?php if ( have_rows( 'cadastro_de_videos' ) ) : ?>
						<?php while ( have_rows( 'cadastro_de_videos' ) ) : the_row(); ?>
							<?php if ( get_sub_field( 'videos' ) ) : ?>
								<div class="video-galery col-md-4">
									<li>
										<video width="100%" height="100%" controls>
											<source src="<?php the_sub_field( 'videos' ); ?>" type="video/mp4">
										</video>
									</li>
								</div>
							<?php else: ?>
								<div class="video-galery col-md-4">
									<li>
										<?php the_sub_field( 'link_youtube' ); ?>
									</li>
								</div>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php else : ?>
						<?php // no rows found ?>
					<?php endif; ?>
					</ul>
			</div>
		</div>
	</div>
</section>
 
<?php get_footer(); ?>