<?php
/**
*
* Template Name: Home
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="capitacao">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12 mb-4">
				<h2><?php the_field( 'titulo_capacitacao' ); ?></h2>
			</div>
			<div class="col-md-12">
				<?php the_field( 'descricao_capacitacao' ); ?><hr>
				<?php $botao_capacitacao = get_field( 'botao_capacitacao' ); ?>
				<?php if ( $botao_capacitacao ) : ?>
					<a href="<?php echo esc_url( $botao_capacitacao['url'] ); ?>" target="<?php echo esc_attr( $botao_capacitacao['target'] ); ?>">
						<button class="btn btn_secondary mt-4 mx-auto mb-4"><?php echo esc_html( $botao_capacitacao['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="transformacao">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_transformacao' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_transformacao' ); ?></p><hr>
			</div>
			
			<div class="col-md-12 pt-3 galery">
				<?php if ( have_rows( 'galerias_transformacao' ) ) : ?>
					<?php while ( have_rows( 'galerias_transformacao' ) ) : the_row(); ?>
						<ul class="galeria-carousel">
							<div class="col-md-12 p-0">
								<h6><?php the_sub_field( 'titulo_galeria' ); ?></h6>
							</div>
							<?php $galeria_transformacao_urls = get_sub_field( 'galeria_transformacao' ); ?>
							<?php if ( $galeria_transformacao_urls ) :  ?>
								<?php foreach ( $galeria_transformacao_urls as $galeria_transformacao_url ): ?>
									<li><img src="<?php echo esc_url( $galeria_transformacao_url ); ?>" /></li>
								<?php endforeach; ?>
							<?php else: ?>
							<hr>
								<p>Em breve</p>							
							<?php endif; ?>
						</ul>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div>
			<div class="col-md-12">
				<?php $botao_transformacao = get_field( 'botao_transformacao' ); ?>
				<?php if ( $botao_transformacao ) : ?>
					<a href="<?php echo esc_url( $botao_transformacao['url'] ); ?>" target="<?php echo esc_attr( $botao_transformacao['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_transformacao['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="video">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12 pb-4">
				<h2><?php the_field( 'titulo_video' ); ?></h2><hr>
				<?php the_field( 'subtitulo_video' ); ?><hr>
			</div>
			<div class="col-md-12 list-video video-carousel">
				<?php if ( have_rows( 'videos' ) ) : ?>
					<?php while ( have_rows( 'videos' ) ) : the_row(); ?>
						<?php if ( get_sub_field( 'video' ) ) : ?>
							<div class="col-md-6">
								<video width="100%" height="100%" controls>
									<source src="<?php the_sub_field( 'video' ); ?>" type="video/mp4">
								</video>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div>	
			<div class="col-md-12 ">
				<?php $botao_video = get_field( 'botao_video' ); ?>	
				<?php if ( $botao_video ) : ?>
					<a href="<?php echo esc_url( $botao_video['url'] ); ?>" target="<?php echo esc_attr( $botao_video['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_video['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>		
		</div>
	</div>
</section>

<!--<section class="noticia">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12 pb-4">
				<h2><?php /*the_field( 'titulo_noticias' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_noticias' ); ?></p><hr>	
			</div>

			<?php get_template_part( 'templates/global/template-part', 'noticias' ); ?>
			<div class="col-md-12 p-0">
				<?php $botao_noticia = get_field( 'botao_noticia' ); ?>
				<?php if ( $botao_noticia ) : ?>
					<a href="<?php echo esc_url( $botao_noticia['url'] ); ?>" target="<?php echo esc_attr( $botao_noticia['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_noticia['title'] ); ?></button>
					</a>
				<?php endif; */?>
			</div>
		</div>
	</div>
</section>-->

<section class="novidades">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-6 box">
				<div class="box-img">
					<?php $imagem_novidade = get_field( 'imagem_novidade' ); ?>
					<?php if ( $imagem_novidade ) : ?>
						<img src="<?php echo esc_url( $imagem_novidade['url'] ); ?>" alt="<?php echo esc_attr( $imagem_novidade['alt'] ); ?>" />
					<?php endif; ?>
				</div>
				<div class="box-content">
					<h2><?php the_field( 'titulo_novidade' ); ?></h2><hr>
					<?php the_field( 'descricao_novidade' ); ?>
				</div>
			</div>
			<div class="col-md-6">
				<?php echo do_shortcode( '[contact-form-7 id="209" title="Receba novidades"]' ); ?>
			</div>
		</div>
	</div>
</section> 

<?php get_template_part( 'templates/global/template-part', 'avalie' ); ?>

<section class="aderir">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12 aderir-box">
				<h2><?php the_field( 'titulo_aderir' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_aderir' ); ?></p>
				<hr>
				<?php $botao_aderir = get_field( 'botao_aderir' ); ?>
				<?php if ( $botao_aderir ) : ?>
					<a href="<?php echo esc_url( $botao_aderir['url'] ); ?>" target="<?php echo esc_attr( $botao_aderir['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_aderir['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<!--<section class="faq">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php /*the_field( 'titulo_faq' ); ?></h2><hr>
			</div>
			<div class="col-md-12 faq-list">
				<?php if ( have_rows( 'duvidas_faq' ) ) : ?>
					<?php while ( have_rows( 'duvidas_faq' ) ) : the_row(); ?>
						<div class="pergunta">
							<?php the_sub_field( 'pergunta' ); ?>
						</div>
						<div class="resposta">
							<?php the_sub_field( 'resposta' ); ?>
						</div>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; */?>
			</div>
		</div>
	</div>
</section>-->

<?php get_footer(); ?>