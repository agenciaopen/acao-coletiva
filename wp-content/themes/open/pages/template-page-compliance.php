<?php
/**
*
* Template Name: Capacitação em Compliance
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="compliance">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <?php the_field( 'descricao_compliance' ); ?>
            </div>
        </div>
    </div>
</section>

<section class="treinamento" id="treinamento">   
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12 pb-4 content-title">
                <h2><?php the_field( 'titulo_treinamento',  ); ?></h2>
                <p><?php the_field( 'subtitulo_treinamento' ); ?></p><hr>
            </div>
        </div>
        <div class="col-md-12 content-video video-carousel">
            <?php if ( have_rows( 'videos_treinamanto' ) ) : ?>
                <?php while ( have_rows( 'videos_treinamanto' ) ) : the_row(); ?>
                    <?php if ( get_sub_field( 'video' ) ) : ?>
                        <div class="col-md-6">
                            <video width="100%" height="100%" controls>
                                <source src="<?php the_sub_field( 'video' ); ?>" type="video/mp4">
                            </video>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
        </div>
        <?php $botao_treinamento = get_field( 'botao_treinamento' ); ?>
        <?php if ( $botao_treinamento ) : ?>
            <a href="<?php echo esc_url( $botao_treinamento['url'] ); ?>" target="<?php echo esc_attr( $botao_treinamento['target'] ); ?>">
                <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_treinamento['title'] ); ?></button></a>
        <?php endif; ?>
    </div>
</section>
 
<?php get_template_part( 'templates/global/template-part', 'cartilhas' ); ?>

<section class="web" id="noticias-e-artigos">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12 pb-4">
                <h2><?php the_field( 'titulo_na_web' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_na_web' ); ?></p><hr>
            </div>
            <?php get_template_part( 'templates/global/template-part', 'noticias' ); ?>
        </div>
        <div class="col-md-12 p-0">
            <?php $botao_web = get_field( 'botao_web' ); ?>
            <?php if ( $botao_web ) : ?>
                <a href="<?php echo esc_url( $botao_web['url'] ); ?>" target="<?php echo esc_attr( $botao_web['target'] ); ?>">
                    <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_web['title'] ); ?></button>
                </a>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_template_part( 'templates/global/template-part', 'avalie' ); ?>

<?php get_footer(); ?>