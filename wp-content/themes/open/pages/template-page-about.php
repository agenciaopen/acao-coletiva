<?php
/**
*
* Template Name: Quem Somos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="about" id="missao-e-valores">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <?php the_field( 'descricao_quem_somos' ); ?><hr>

                <h2><?php the_field( 'titulo_nossos_valores' ); ?></h2><hr>
                <ul class="valores">
                    <?php if ( have_rows( 'cadastro_valores' ) ) : ?>
                        <?php while ( have_rows( 'cadastro_valores' ) ) : the_row(); ?>
                            <li>
                                <?php $icone = get_sub_field( 'icone' ); ?>
                                <?php if ( $icone ) : ?>
                                    <img src="<?php echo esc_url( $icone['url'] ); ?>" alt="<?php echo esc_attr( $icone['alt'] ); ?>" />
                                <?php endif; ?>
                                <p><?php the_sub_field( 'descricao_icone' ); ?></p>
                            </li>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </ul>
                <hr>
                <?php $botao_quem_somos = get_field( 'botao_quem_somos' ); ?>
                <?php if ( $botao_quem_somos ) : ?>
                    <a href="<?php echo esc_url( $botao_quem_somos['url'] ); ?>" target="<?php echo esc_attr( $botao_quem_somos['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_quem_somos['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="movimento" id="movimento">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_movimento' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_movimento' ); ?></p><hr>
			</div>
			<div class="col-md-12 galery">
				<?php if ( have_rows( 'galerias_movimento' ) ) : ?>
					<?php while ( have_rows( 'galerias_movimento' ) ) : the_row(); ?>
						<ul class="parceiros-carousel">
							<div class="col-md-12 p-0">
								<h6><?php the_sub_field( 'titulo_galeria' ); ?></h6>
							</div>
							<?php $galeria_movimento_urls = get_sub_field( 'galeria_movimento' ); ?>
							<?php if ( $galeria_movimento_urls ) :  ?>
								<?php foreach ( $galeria_movimento_urls as $galeria_movimento_url ): ?>
									<li>
										<img src="<?php echo esc_url( $galeria_movimento_url ); ?>" />
									</li>
								<?php endforeach; ?>
							<?php else: ?>
								<hr>
								<p>Em breve</p>	
							<?php endif; ?>
						</ul><hr>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
			</div>
			<div class="col-md-12">
				<?php $botao_movimento = get_field( 'botao_movimento' ); ?>
				<?php if ( $botao_movimento ) : ?>
					<a href="<?php echo esc_url( $botao_movimento['url'] ); ?>" target="<?php echo esc_attr( $botao_movimento['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_movimento['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="parceiros" id="parceiros">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<h2><?php the_field( 'titulo_parceiros' ); ?></h2><hr>
				<p><?php the_field( 'subtitulo_parceiros' ); ?></p><hr>
			</div>
			<div class="col-md-12 pt-3 galery">
				<ul class="parceiros-carousel">
					<?php $galeria_parceiros_urls = get_field( 'galeria_parceiros' ); ?>
					<?php if ( $galeria_parceiros_urls ) :  ?>
						<?php foreach ( $galeria_parceiros_urls as $galeria_parceiros_url ): ?>
							<li>
								<img src="<?php echo esc_url( $galeria_parceiros_url ); ?>" />
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
					<div class="col-md-12 p-0">
						<?php the_field( 'descricao_galeria' ); ?>
					</div>
				</ul>
			</div>
			<div class="col-md-12">
				<?php $botao_parceiros = get_field( 'botao_parceiros' ); ?>
				<?php if ( $botao_parceiros ) : ?>
					<a href="<?php echo esc_url( $botao_parceiros['url'] ); ?>" target="<?php echo esc_attr( $botao_parceiros['target'] ); ?>">
						<button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_parceiros['title'] ); ?></button>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
 
<?php get_footer(); ?>