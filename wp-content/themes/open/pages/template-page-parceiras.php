<?php
/**
*
* Template Name: Parceiras
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="description">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <?php the_field( 'descricao_parceiras' ); ?>
            </div>
        </div>
    </div>
</section>

<section class="conhecer">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-conhecer conhecer-carousel">
                    <?php if ( have_rows( 'box_conhecer' ) ) : ?>
                        <?php while ( have_rows( 'box_conhecer' ) ) : the_row(); ?>
                            <li class="card-box">
                                <div class="box-img">
                                    <?php $imagen = get_sub_field( 'imagen' ); ?>
                                    <?php if ( $imagen ) : ?>
                                        <img src="<?php echo esc_url( $imagen['url'] ); ?>" alt="<?php echo esc_attr( $imagen['alt'] ); ?>" />
                                    <?php endif; ?>
                                </div>
                                <div class="box-content">
                                    <?php $botao = get_sub_field( 'botao' ); ?>
                                    <?php if ( $botao ) : ?>
                                        <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>">
                                            <button class="btn btn_first mx-auto"><?php echo esc_html( $botao['title'] ); ?></button>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="informacao" style="background-image:url('<?php echo get_field( 'imagem_informacao' ); ?>');">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-5">
                <h2><?php the_field( 'titulo_informacao' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_informacao' ); ?></p><hr>
                <?php $botao_informacao = get_field( 'botao_informacao' ); ?>
                <?php if ( $botao_informacao ) : ?>
                    <a href="<?php echo esc_url( $botao_informacao['url'] ); ?>" target="<?php echo esc_attr( $botao_informacao['target'] ); ?>">
                        <button class="btn btn_first mx-auto"><?php echo esc_html( $botao_informacao['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                
            </div>
        </div>
    </div>
</section> 







<?php get_footer(); ?>