<?php
/**
*
* Template Name: Login 
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>


<section class="account">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12 pb-5">
                    <h2><?php the_field( 'titulo_acesso' ); ?></h2><hr>
                    <p><?php the_field( 'subtitulo_acesso' ); ?></p>
                </div>
            <div class="account-login col-md-6">
                <h2>Ja é cadastrado? Faça o login.</h2><hr>
                <div class="col-md-10 form">
                    <?php $args = array(
                        'echo' => true,
                        'redirect' => 'http://acao.open',
                        'form_id' => 'loginform',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember Me' ),
                        'label_log_in' => __( 'Log In' ),
                        'id_username' => 'user_login',
                        'id_password' => 'user_pass',
                        'id_remember' => 'rememberme',
                        'id_submit' => 'wp-submit',
                        'remember' => true,
                        'value_username' => NULL,
                        'value_remember' => false );
                        wp_login_form($args);
                    ?>
                </div>
            </div>
            <div class="accont-create col-md-6">
                <h2>Ainda não possui cadastro?</h2><hr>
                <?php echo do_shortcode('[contact-form-7 id="279" title="Registro de usuário"]');?>
            </div>
        </div>
</section><!--/.account-->


<?php get_footer(); ?>