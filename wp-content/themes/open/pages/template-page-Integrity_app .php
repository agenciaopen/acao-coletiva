<?php
/**
*
* Template Name: Integrity App 
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="integrity-app">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field( 'titulo_integrity_app_' ); ?></h2><hr>
            </div>
            <div class="col-md-9">
                <?php the_field( 'descricao_integrity_app' ); ?>
            </div>
            <div class="col-md-3 plataformas">
                <?php if ( have_rows( 'plataformas_app' ) ) : ?>
                    <?php while ( have_rows( 'plataformas_app' ) ) : the_row(); ?>
                        <?php $logo_plataforma = get_sub_field( 'logo_plataforma' ); ?>
                        <?php if ( $logo_plataforma ) : ?>
                            <img src="<?php echo esc_url( $logo_plataforma['url'] ); ?>" alt="<?php echo esc_attr( $logo_plataforma['alt'] ); ?>" /><hr>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
            <div class="col-md-12">
                <?php $botao_app = get_field( 'botao_app' ); ?>
                <?php if ( $botao_app ) : ?>
                    <a href="<?php echo esc_url( $botao_app['url'] ); ?>" target="<?php echo esc_attr( $botao_app['target'] ); ?>">
                    <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_app['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="faq">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field( 'titulo_duvidas' ); ?></h2><hr>
            </div>
            <div class="col-md-12 list-faq">
                <?php if ( have_rows( 'perguntas_e_respostas' ) ) : ?>
                    <?php while ( have_rows( 'perguntas_e_respostas' ) ) : the_row(); ?>
                        <div class="box-perguntas">
                            <p class="pergunta"><?php the_sub_field( 'perguntas' ); ?><i class="arrow down"></i></p><hr>
                            <div class="box">
                                <?php the_sub_field( 'respostas' ); ?><hr>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<!--<?php /*<section class="agenda-de-eventos" style="background-image:url('<?php echo get_field( 'imagem_agenda', 'option' ) ?>');">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-5">
                <h2><?php the_field( 'titulo_agenda', 'option' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo_agenda', 'option' ); ?></p><hr>
                <?php $botao_agenda = get_field( 'botao_agenda', 'option' ); ?>
                <?php if ( $botao_agenda ) : ?>
                    <a href="<?php echo esc_url( $botao_agenda['url'] ); ?>" target="<?php echo esc_attr( $botao_agenda['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao_agenda['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
            <div class="col-md-7">
            </div>
        </div>
    </div>
</section> */?>-->   

<?php get_footer(); ?>