<?php
/**
*
* Template Name: Sala de Imprensa
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'templates/global/template-part', 'banner' ); ?>

<section class="contato-assessoria">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field( 'titulo_assessoria' ); ?></h2><hr>
                <ul>
                    <?php if ( have_rows( 'contato_assessoria' ) ) : ?>
                        <?php while ( have_rows( 'contato_assessoria' ) ) : the_row(); ?>
                            <li><p><?php the_sub_field( 'nome' ); ?>: <?php the_sub_field( 'telefone' ); ?> | <?php the_sub_field( 'e-mail' ); ?></p></li>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="realeases">
    <div class="container h-100">
        <div class="row">
            <div class="col-md-12">
                <h2><?php the_field( 'title_realeases' ); ?></h2><hr>
                <p><?php the_field( 'subtitulo' ); ?></p><hr>
            </div>
            <div class="col-md-12">
                <?php if ( have_rows( 'releases' ) ) : ?>
                    <?php while ( have_rows( 'releases' ) ) : the_row(); ?>
                        <div class="box-container pb-4 pt-5">
                            <div class="box">
                                <h2><?php the_sub_field( 'titulo' ); ?></h2><hr>
                                <?php the_sub_field( 'descricao' ); ?><hr>
                            </div>
                            <?php $botao = get_sub_field( 'botao' ); ?>
                            <?php if ( $botao ) : ?>
                                <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>">
                                    <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao['title'] ); ?></button>
                                </a>
                            <?php endif; ?> 
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

















<?php get_footer(); ?>