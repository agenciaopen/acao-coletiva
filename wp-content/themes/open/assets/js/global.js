
       var header = $("#nav_main");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                header.addClass("scrolled");
            } else {
                header.removeClass("scrolled");
            }
        });
        // $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
        //     $(".navbar-collapse").addClass("show");
        // })
          
        // $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function () {
        //     $(".navbar-collapse").removeClass("show");
        // })
        // $(".menu-item-has-children .dropdown-toggle").html("<div id='nav-icon-item'><span id='nav-icon3'><span></span><span></span><span></span><span></span></span></div>");
        // $('#nav_main .dropdown-toggle').click(function(){
        //     $('#nav_main').toggleClass('nav-open');
        //     $('footer').removeClass('nav-open');

        // });
        // $('footer .dropdown-toggle').click(function(){
        //     $('#nav_main').removeClass('nav-open');
        //     $('footer').toggleClass('nav-open');
        // });
        // $("footer .dropdown").addClass("dropup");
       
        /*==============================================================
        navbar fixed top
        ==============================================================*/
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 100;
        var navbarHeight = $('#nav_main').outerHeight();

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $('.navbar').removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('.navbar').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }

// var behavior = function (val) {
//     return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
// },
//     options = {
//         onKeyPress: function (val, e, field, options) {
//             field.mask(behavior.apply({}, arguments), options);
//         }
//     };

// $('input.tel, input.wpcf7-tel').mask(behavior, options);

// $('.carousel_testimonials').slick({
//     dots: true,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 1,
//     arrows: false,
//     slidesToScroll: 1,
//     dots: true,
//     infinite: true,
//     cssEase: 'linear',
//     adaptiveHeight: true
// });


/*
accordion
*/
$('#accordion .collapse').on('shown.bs.collapse', function (e) {
    $(this).prev().addClass('active');
    var $card = $(this).closest('.card');
    var $open = $($(this).data('parent')).find('.collapse.show');
    var additionalOffset = 60;
    if($card.prevAll().filter($open.closest('.card')).length !== 0)
    {
          additionalOffset =  $open.height();
    }
    $('html,body').animate({
      scrollTop: $card.offset().top - additionalOffset
    }, 500);
});
$('#accordion .collapse').on('hidden.bs.collapse', function () {
    $(this).prev().removeClass('active');
});


/*$('.parceiros-carousel').slick({
    slidesPerRow: 5,
    rows: 2,
    responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesPerRow: 2,
        rows: 6,
      }
    }
  ]
});

$('.conhecer-carousel').slick({
    slidesPerRow: 3,
    rows: 2,
    responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesPerRow: 1,
        rows: 6,
      }
    }
  ]
});*/

$('.video-carousel').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 769,
      settings: {
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
  ]
});

$(function(){
  $('.faq .list-faq .box-perguntas p.pergunta').click(function(){
    $(this).siblings('div.box').slideToggle();
      $(this).find('i').toggleClass('arrow up arrow down');
  })
})

$('a').click(function(){
  $('html, body').animate({
      scrollTop: $( $(this).attr('href') ).offset().top
  }, 500);
  return false;
});