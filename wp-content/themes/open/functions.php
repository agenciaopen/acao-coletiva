<?php

/**
 * Register theme initial settings
*/
require_once('inc/theme-settings.php');

/**
 * Register required plugins
*/
require_once('inc/plugins/required.php');

/**
 * Register acf initial settings
*/
require_once('inc/theme-acf.php');


/**
 * Register Custom Navigation Walker
*/
require_once('inc/theme-menu.php');

/**
 * Enqueue global styles and scripts
*/
require_once('inc/theme-enqueue.php');


/**
 * Remove wp tags
*/
require_once('inc/remove-wp.php');

/**
* Custom login page
*/
require_once('inc/login-page.php');

/**
* Custom admin page
*/
require_once('inc/wp-admin.php');

/**
* Upload SVG files
*/
require_once('inc/svg-upload.php');

/**
* Remove emojis
*/
require_once('inc/remove-emojis.php');


/**
* Compress all html except for admin
* -----> CAREFUL USE <------
*/
if (!current_user_can('administrator') && !is_admin()) {
    require_once('inc/compress-html.php');
}

/**
* Remove Gutenberg
*/
require_once('inc/remove-gutenberg.php');



add_filter( 'wpcf7_validate_password*', 'custom_password_confirmation_validation_filter', 20, 2 );
  
function custom_password_confirmation_validation_filter( $result, $tag ) {
  if ( 'your-password-confirm' == $tag->name ) {
    $your_password = isset( $_POST['your-password'] ) ? trim( $_POST['your-password'] ) : '';
    $your_password_confirm = isset( $_POST['your-password-confirm'] ) ? trim( $_POST['your-password-confirm'] ) : '';
  
    if ( $your_password != $your_password_confirm ) {
      $result->invalidate( $tag, "Senha não confere" );
    }
  }
  
  return $result;
}

function wpm_create_user_form_registration( $cfdata ) {
    if ( ! isset( $cfdata->posted_data ) && class_exists( 'WPCF7_Submission' ) ) {
        // Contact Form 7 version 3.9 removed $cfdata->posted_data and now
        // we have to retrieve it from an API
        $submission = WPCF7_Submission::get_instance();
        if ( $submission ) {
            $formdata = $submission->get_posted_data();
        }
    } elseif ( isset( $cfdata->posted_data ) ) {
        // For pre-3.9 versions of Contact Form 7
        $formdata = $cfdata->posted_data;
    } else {
        // We can't retrieve the form data
        return $cfdata;
    }
//     // Check this is the user registration form
    if ( $cfdata->title() == 'Registro de usuário' ) {
        $username = $formdata['your-email'];
        $email    = $formdata['your-email'];
        $password = $formdata['your-password-confirm'];
        $fname    = $formdata['your-name'];
        $lname    = $formdata['LNAME'];
 
        if ( ! email_exists( $email ) ) {
            // Find an unused username
            $username_tocheck = $username;
            $i                = 1;
            while ( username_exists( $username_tocheck ) ) {
                $username_tocheck = $username . $i ++;
            }
            $username = $username_tocheck;
            // Create the user
            $userdata = array(
                'user_login'   => $username,
                'user_pass'    => $password,
                'user_email'   => $email,
                'nickname'     => $fname . ' ' . $lname,
                'display_name' => $fname . ' ' . $lname,
                'first_name'   => $fname,
                'last_name'    => $lname,
                'role'         => 'subscriber'
            );
 
            $user_id = wp_insert_user( $userdata );
 
            if ( ! is_wp_error( $user_id ) ) {
                wp_set_current_user( $user_id );
                wp_set_auth_cookie( $user_id );
 
                do_action( 'woocommerce_created_customer', $user_id );
            }
      
        }
    }
 
    return $cfdata;
}
add_action( 'wpcf7_before_send_mail', 'wpm_create_user_form_registration', 1 );

?>
