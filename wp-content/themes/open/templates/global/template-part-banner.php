<?php
global $post;
$page_ID = $post->ID;
// get page ID
 
if ( wp_is_mobile() ) :
        $featured_img_url = get_field( 'banner_mobile', $page_ID );
else :
	    $featured_img_url = get_field( 'banner_desktop', $page_ID );
endif;

?>
<?php if( get_field('titulo_principal', $page_ID) ): ?>
    <?php $title = get_field('titulo_principal', $page_ID); ?>
<?php else: ?>
    <?php $title = get_the_title(); ?>
<?php endif; ?>

<section class="main" style="background-image:url('<?php echo $featured_img_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-12">
                    <h1><?php echo $title;?></h1>
                    <hr>
                    <h6><?php the_field( 'descricao_principal', $page_ID, false, false); ?></h6>
                <?php $botao = get_field( 'botao', $page_ID ); ?>
                <?php if ( $botao ) : ?>
                    <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao['title'] ); ?></button>
                    </a>
                <?php endif; ?><hr>
                <h6><?php the_field( 'descricao_secundaria' ); ?></h6>
            </div>
            <div class="col-md-10 align-itens-center justify-content-center d-none">
                <?php if (is_page_template('pages/template-page-home.php')): ?>
                <div class="cookies">
                    <p>
                        Nós usamos cookies e tecnologias semelhantes para melhorar sua experiência neste site, 
                        recomendar conteúdo de seu interesse e personalizar publicidade. Ao continuar, você concorda com este 
                        monitoramento.  
                    </p>
                    <div class="buttons">
                        <button class="btn btn_secondary mt-4 mr-3">VOLTAR</button>
                        <button class="btn btn_first mt-4">CONTINUAR</button>
                    </div>
                </div>
                
                <?php endif;?>
            </div>
        </div>
    </div>
    <?php if (is_page_template('template-page-obras.php')) : ?>
        
    <?php endif; ?>
</section><!-- /.main -->