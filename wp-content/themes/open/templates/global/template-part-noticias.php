<div class="col-md-12 p-0 posts-noticias">
    <ul class="list-post">
        <?php
            $args_query = array(
            'post_status' => array('publish'),
            'posts_per_page' => 3,
            'post_type' => 'post',
            'order' => 'ASC',
        );

        // The Query
        $query = new WP_Query($args_query);

        // The Loop
        if ($query->have_posts()) {
            $cont = 0;
            while ($query->have_posts()) {
                $query->the_post();
                // Your custom code 
        ?>
                <li class="card col-md-4 box-card-<?php echo $cont; ?>">
                    <div class="row col-md-12 p-0 m-0">
                        <div class="card-header order-2 order-md-1">
                            <h3><?php the_title(''); ?></h3>
                            <span class="date"><?php the_date(); ?></span><hr>
                        </div>
                        <div class="card-img order-1 order-md-3">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>">
                            <hr>
                        </div>
                        <div class="card-content pl-0 col-md-10 order-3 order-md-3">
                            <p><?php the_field( 'descricao_do_post' ); ?></p><hr>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                CONTINUAR LENDO
                            </a>
                        </div>
                    </div>
                </li>
        <?php $cont++;                            }
        } else {
            // no posts found

        }
        /* Restore original Post Data */
        wp_reset_postdata();
        ?>
    </ul>
</div>